This image serves as a backup in case the original source that is being downloaded is removed.

This has happened before. It is not used in the current Dockerfile as of now, but can be quickly put into action if needed.