How the execution works:

The file `run.py` is called. It prepares the environment: copies S2E files over, optionally installs R libraries, and calls S2E (via s2e run ... or executing `s2e-launch.sh`).

Files for S2E include the `bootstrap.sh`, describing how the environment in QEMU should be configured, namely which files should be transferred and which command should be ran. The `generate-dirs.sh` generates list of files that are to be transmitted over to the VM, the resulting file is read by `bootstrap.sh`

The list of files contain for example the R interpreter, the source code and installed libraries.

The `s2e-config.lua` describes default configuration. The difference to S2E default is inclusion of Chef. The user can overwrite these defaults by placing `s2e-config.lua` in the directory that gets mounted into `/data`, so the root one (while running the image, not building it).
