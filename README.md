# Chef-nextgen

Bachelor's thesis.

See dev folder for development docker setup. This allows one to quickly prototype and use JetBrains IDEs.

See main folder for release docker setup. This is a docker image that allows one to easily use Chef.

Furthermore, this repository contains docker images for running Chef and builds of the python tooling.
