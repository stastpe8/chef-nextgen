#!/bin/sh
find build/lib -type d | sed 's/^build\///' > r-dirs.txt
find build/library -type d | sed 's/^build\///' >> r-dirs.txt
find build/etc -type d | sed 's/^build\///' >> r-dirs.txt
find build/lib -type f | sed 's/^build\///' > r-files.txt
find build/library -type f | sed 's/^build\///' >> r-files.txt
find build/etc -type f | sed 's/^build\///' >> r-files.txt
