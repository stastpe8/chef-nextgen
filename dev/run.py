#!/usr/bin/python3

import shutil
import signal
import subprocess
import sys
from pathlib import Path


def parse_args(args: list[str]) -> (str, str, list[str]):
    """
    Parse args. If returns, returns tuple (program to load, output dir, [libraries to install])
    """
    if "-h" in args or "--help" in args or len(args) < 2:
        print(
            'Use like: docker run -v "$PWD":/data <docker-image> run <R-source> <Output-dir> [libraries to install]'
        )
        print("As an example:")
        print(
            'docker run -v "$PWD":/data r-symbex:latest testBase64EncodingEquiv.R results/equiv base64enc jsonlite'
        )
        print("You do not need to specify any libraries if you need just to base ones")
        sys.exit(0)

    return args[0], args[1], args[2:]


def install_libraries(libs: list[str]) -> None:
    if len(libs) == 0:
        return

    print(f"[+] Installing {len(libs)} libraries. This will take a while", flush=True)

    lib_vec = "c(" + ",".join([f'"{x}"' for x in libs]) + ")"

    subprocess.run(
        [
            "build/bin/Rscript",
            "-e",
            f'install.packages({lib_vec}, repos="https://cloud.r-project.org/")',
        ],
        check=True,
    )


def setup_folders() -> None:
    print("[+] Generating folder structure for S2E.", flush=True)
    subprocess.run(["./generate-dirs.sh"], check=True)


def launch_s2e(source: str) -> None:
    # Copy config if there is one
    config_file = Path(f"/data/s2e-config.lua")
    if config_file.exists():
        print("[+] User-provided configuration detected, overwriting the default one.", flush=True)
        shutil.copy(config_file, Path.cwd())


    # Copy source code
    shutil.copy(f"/data/{source}", "source.R")
    print("[+] Launching S2E. Press Ctrl+C to stop it prematurely.", flush=True)
    try:
        p = subprocess.Popen(
            ["/bin/bash", "-c", ". /home/s2e/s2e/s2e_activate ; ./launch-s2e.sh"]
        )
        p.wait()
    except KeyboardInterrupt:
        print("[+] Stopping S2E", flush=True)
        try:
            p.send_signal(signal.SIGINT)
            p.wait()
        except KeyboardInterrupt:
            print("[+] Killing S2E", flush=True)
            p.kill()


def copy_result(output_dir: str) -> None:
    print("[+] Copying results", flush=True)
    parent_dir = Path(output_dir).parent
    parent_dir.mkdir(parents=True, exist_ok=True)
    shutil.copytree("s2e-out-0", f"/data/{output_dir}")
    shutil.copy("serial.txt", f"/data/{output_dir}/serial.txt")


def check_validity(source_name: str, output_dir: str) -> None:
    if not (Path("/data") / Path(source_name)).exists():
        print(
            "Cannot open input file. Did you mount the directory with it as /data into the container? See -h for help."
        )
        sys.exit(1)
    if (Path("/data") / Path(output_dir)).exists():
        print(
            f"Output directory {str(output_dir)} already exists. Delete it or choose another name."
        )
        sys.exit(1)


def main():
    arguments = sys.argv[1:]

    source_name, output_dir, libs = parse_args(arguments)

    check_validity(source_name, output_dir)
    install_libraries(libs)
    setup_folders()
    launch_s2e(source_name)
    copy_result(output_dir)


if __name__ == "__main__":
    main()
