This folder contains backup of development environment, usable for quick recovery from the docker-compose file. With these, everything should work out of the box.

It should be noted that these are *not* up-to-date with respect to changes made in the modified R interpreter and S2E, so the user should update these projects (`git pull`) once the environment is set up.
